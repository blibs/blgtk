#include "blgtk.h"

int blgtk_is_toggled(GtkToggleToolButton *toggle_button)
{
    return gtk_toggle_tool_button_get_active(toggle_button);
}

int blgtk_is_toggled(GtkToggleButton *toggle_button)
{
    return gtk_toggle_button_get_active(toggle_button);
}

GtkWidget *blgtk_add_tool_button(GtkWidget *toolbar, gchar const *label, gchar const *icon_name, GCallback callback, gpointer user_data)
{
    GtkWidget *tool_button;
    
    tool_button=GTK_WIDGET(gtk_tool_button_new(gtk_image_new_from_icon_name(icon_name, GTK_ICON_SIZE_SMALL_TOOLBAR), label));
    gtk_container_add(GTK_CONTAINER(toolbar), tool_button);
    if (callback) g_signal_connect(G_OBJECT(tool_button), "clicked", callback, user_data);
    return tool_button;
}

GtkToggleToolButton *blgtk_add_toggle_tool_button(GtkWidget *toolbar, char const *label, char const *icon_name, GCallback callback, void *user_data)
{
    GtkToggleToolButton *toggle_tool_button;
    
    toggle_tool_button=GTK_TOGGLE_TOOL_BUTTON(gtk_toggle_tool_button_new());
    
    gtk_tool_button_set_icon_name(GTK_TOOL_BUTTON(toggle_tool_button), icon_name);
    gtk_tool_button_set_label(GTK_TOOL_BUTTON(toggle_tool_button), label);
    gtk_container_add(GTK_CONTAINER(toolbar), GTK_WIDGET(toggle_tool_button));
    if (callback) g_signal_connect(G_OBJECT(toggle_tool_button), "clicked", callback, user_data);
    return toggle_tool_button;
}

GtkWidget *blgtk_add_menu_tool_button(GtkWidget *toolbar, gchar const *label, gchar const *icon_name, GtkWidget *menu)
{
    GtkWidget *menu_tool_button;
    
    menu_tool_button=GTK_WIDGET(gtk_menu_tool_button_new(gtk_image_new_from_icon_name(icon_name, GTK_ICON_SIZE_SMALL_TOOLBAR), label));
    gtk_menu_tool_button_set_menu(GTK_MENU_TOOL_BUTTON(menu_tool_button), menu);
    gtk_container_add(GTK_CONTAINER(toolbar), menu_tool_button);

    return menu_tool_button;
}
