find_package(blc_image REQUIRED)

find_path(BLGTK_INCLUDE_DIR blgtk.h PATH_SUFFIXES blgtk)
find_library(BLGTK_LIBRARY blgtk)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(blc_image DEFAULT_MSG  BLGTK_LIBRARY BLGTK_INCLUDE_DIR)

mark_as_advanced(BLGTK_INCLUDE_DIR BLGTK_LIBRARY )

set(BL_INCLUDE_DIRS ${BLGTK_INCLUDE_DIR} ${BL_INCLUDE_DIRS} )
set(BL_LIBRARIES ${BLGTK_LIBRARY} ${BL_LIBRARIES} )

